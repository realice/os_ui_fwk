# OH窗口管理

## 关键模块类图

### Client端
#### DM
<img src="./out/client/dm/dm.svg">

#### WM
<img src="./out/client/wm/wm.svg">

### Server端
#### DMS
<img src="./out/server/dms/dms.svg">

#### WMS
<img src="./out/server/wms/wms.svg">

### IPC
#### IPC基础
<img src="./out/binder/base/base.svg">

#### IDisplayManager-Agent
<img src="./out/binder/service/IDisplayManager-Agent/IDisplayManager-Agent.svg">

#### IWindowManager-Agent
<img src="./out/binder/service/IWindowManager-Agent/IWindowManager-Agent.svg">

#### IWindowExtension-Client
<img src="./out/binder/IWindowExtension-Client/IWindowExtension-Client.svg">

#### IWindow
<img src="./out/binder/IWindow/IWindow.svg">
