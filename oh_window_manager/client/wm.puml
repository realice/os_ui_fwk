@startuml wm

JsWindowStage *- WindowScene
WindowScene ..> IWindowLifeCycle
WindowScene -> WindowOption
WindowScene ..> SystemBarProperty
WindowScene *- Window

Window ..> WindowOption
Window -> IWindowLifeCycle

Window <|-- WindowImpl #line:red
WindowAgent *- WindowImpl
WindowImpl ..> WindowOption
WindowImpl *-down- WindowProperty
WindowImpl *-up- WindowState
WindowImpl *-up- WindowTag
WindowImpl *-up- RSSurfaceNode
WindowImpl .up.> InputTransferStation
WindowImpl ..> WindowAdapter
WindowProperty *-down- WindowType
WindowProperty "1" *-down- "2" WindowMode
WindowProperty *-down- WindowBlurLevel
InputTransferStation "1" *-up- "n" WindowInputChannel

JsWindowManager ..> WindowManager
JsWindowRegisterManager ..> WindowManager
WindowManagerAgent ..> WindowManager
WindowManager *- WindowManagerImpl
WindowManager ..> WindowAdapter
WindowManagerImpl ..> WindowAdapter
WindowAdapter *- IWindowManager

class Window #back:Red
interface IWindowLifeCycle {
    void AfterForeground()
    void AfterBackground()
    void AfterFocused()
    void AfterUnfocused()
    void ForegroundFailed()
    void AfterActive()
    void AfterInactive()
}
class WindowImpl #back:LightGreen {
    -static const ColorSpaceConvertMap colorSpaceConvertMap[]

    -static map<string, pair<uint32_t, sptr<Window> windowMap_
    -static map<uint32_t, vector<sptr<WindowImpl> subWindowMap_,appFloatingWindowMap_

	-shared_ptr<VsyncStation::VsyncCallback> callback_
    -sptr<IAceAbilityHandler> aceAbilityHandler_

    -sptr<WindowProperty> property_
    -WindowState state_
    -WindowTag windowTag_

    -ITouchOutsideListener touchOutsideListeners_; IWindowLifeCycle lifecycleListeners_
    -IWindowChangeListener windowChangeListeners_; IAvoidAreaChangedListener avoidAreaChangeListeners_
    -IWindowDragListener windowDragListeners_; IDisplayMoveListener displayMoveListeners_
    -IOccupiedAreaChangeListener occupiedAreaChangeListeners_; IInputEventListener inputEventListeners_

    -NotifyNativeWinDestroyFunc notifyNativefunc_
    -shared_ptr<RSSurfaceNode> surfaceNode_
    -string name_
    -unique_ptr<Ace::UIContent> uiContent_
    -shared_ptr<AbilityRuntime::Context> context_
    -shared_ptr<EventHandler> eventHandler_
    -recursive_mutex mutex_

    -int32_t startPointPosX_,startPointPosY_,startPointerId_
    -bool startDragFlag_,startMoveFlag_,pointEventStarted_
    -Rect startPointRect_,startRectExceptFrame_,startRectExceptCorner_
    -DragType dragType_ = DragType::DRAG_UNDEFINED
    -bool isAppDecorEnbale_ = true
    -SystemConfig windowSystemConfig_ 
    -bool isOriginRectSet_,isWaitingFrame_,needRemoveWindowInputChannel_,isListenerHandlerRunning_
}
class WindowProperty #back:LightGreen {
    string windowName_
    Rect requestRect_, windowRect_
    bool decoStatus_
    WindowType type_
    WindowMode mode_
    WindowBlurLevel level_
    WindowMode lastMode_
    uint32_t flags_
    bool isFullScreen_,focusable_,touchable_,isPrivacyMode_,isTransparent_,tokenState_
    float alpha_, brightness_
    bool turnScreenOn_, keepScreenOn_
    uint32_t callingWindow_
    DisplayId displayId_
    uint32_t windowId_, parentId_
    PointInfo hitOffset_
    uint32_t animationFlag_
    uint32_t modeSupportInfo_
    WindowSizeChangeReason windowSizeChangeReason_
    unordered_map<WindowType, SystemBarProperty> sysBarPropMap_
    bool isDecorEnable_
    Rect originRect_
    bool isStretchable_
    DragType dragType_
    vector<Rect> touchHotAreas_
}
enum WindowMode {
    WINDOW_MODE_UNDEFINED = 0,
    WINDOW_MODE_FULLSCREEN = 1,
    WINDOW_MODE_SPLIT_PRIMARY = 100,
    WINDOW_MODE_SPLIT_SECONDARY,
    WINDOW_MODE_FLOATING,
    WINDOW_MODE_PIP
}
enum WindowBlurLevel {
    WINDOW_BLUR_OFF = 0,
    WINDOW_BLUR_LOW,
    WINDOW_BLUR_MEDIUM,
    WINDOW_BLUR_HIGH    
}
enum WindowType {
    APP_WINDOW_BASE
    ...
    APP_SUB_WINDOW_BASE
    ...
    SYSTEM_WINDOW_BASE
    ...
    BELOW_APP_SYSTEM_WINDOW_BASE
    ...
    ABOVE_APP_SYSTEM_WINDOW_BASE
    ...
}
enum WindowState {
    STATE_INITIAL,
    STATE_CREATED,
    STATE_SHOWN,
    STATE_HIDDEN,
    STATE_FROZEN,
    STATE_DESTROYED,
    STATE_BOTTOM = STATE_DESTROYED,
    STATE_UNFROZEN,
}
enum WindowTag {
    MAIN_WINDOW = 0,
    SUB_WINDOW = 1,
    SYSTEM_WINDOW = 2,
}
class RSSurfaceNode {
    shared_ptr<RSSurface> surface_
    string name_
}
class InputTransferStation {
    -unordered_map<uint32_t, sptr<WindowInputChannel windowInputChannels_
}
class WindowInputChannel {
    -sptr<Window> window_
}
class JsWindowStage {
    weak_ptr<Rosen::WindowScene> windowScene_
}
class WindowScene #back:LightGreen {
    -sptr<Window> mainWindow_
    -static uint32_t count
    -DisplayId displayId_
}
class WindowOption #back:LightGreen {
    -Rect windowRect_
    -WindowType type_
    -WindowMode mode_
    -WindowBlurLevel level_

    -float alpha_
    -bool focusable_
    -bool touchable_

    -DisplayId displayId_
    -string parentName_
    -string windowName_

    -uint32_t flags_
    -PointInfo hitOffset_
    -WindowTag windowTag_
    -bool keepScreenOn_
    -bool turnScreenOn_
    -float brightness_
    -uint32_t callingWindow_
    -unordered_map<WindowType, SystemBarProperty> sysBarPropMap_
    -Orientation requestedOrientation_
}
class SystemBarProperty {
    bool enable_
    uint32_t backgroundColor_
    uint32_t contentColor_
}
class WindowAgent {
    sptr<WindowImpl> window_
}
class WindowManager #back:LightBlue {
    +void RegisterFocusChangedListener(const sptr<IFocusChangedListener>& listener)
    +void UnregisterFocusChangedListener(const sptr<IFocusChangedListener>& listener)
    +void RegisterSystemBarChangedListener(const sptr<ISystemBarChangedListener>& listener)
    +void UnregisterSystemBarChangedListener(const sptr<ISystemBarChangedListener>& listener)
    +void RegisterWindowUpdateListener(const sptr<IWindowUpdateListener>& listener)
    +void UnregisterWindowUpdateListener(const sptr<IWindowUpdateListener>& listener)
    +void RegisterVisibilityChangedListener(const sptr<IVisibilityChangedListener>& listener)
    +void UnregisterVisibilityChangedListener(const sptr<IVisibilityChangedListener>& listener)

    +void MinimizeAllAppWindows(DisplayId displayId)
    +WMError ToggleShownStateForAllAppWindows()
    +WMError SetWindowLayoutMode(WindowLayoutMode mode)
    +WMError GetAccessibilityWindowInfo(sptr<AccessibilityWindowInfo>& windowInfo) const
}
class WindowManagerImpl #back:LightBlue {
    +void PostTask(ListenerTaskCallback &&callback, EventPriority priority, const string name)
    +void InitListenerHandler()

    +void NotifyFocused(uint32_t windowId, const sptr<IRemoteObject>& abilityToken, WindowType windowType, DisplayId displayId)
    +void NotifyUnfocused(uint32_t windowId, const sptr<IRemoteObject>& abilityToken, WindowType windowType, DisplayId displayId)
    +void NotifyFocused(const sptr<FocusChangeInfo>& focusChangeInfo)
    +void NotifyUnfocused(const sptr<FocusChangeInfo>& focusChangeInfo)
    +void NotifySystemBarChanged(DisplayId displayId, const SystemBarRegionTints& tints)
    +void NotifyAccessibilityWindowInfo(const sptr<AccessibilityWindowInfo>& windowInfo, WindowUpdateType type)
    +void NotifyWindowVisibilityInfoChanged(const vector<sptr<WindowVisibilityInfo& windowVisibilityInfos)

    -sptr<WindowManagerAgent> focusChangedListenerAgent_
    -sptr<WindowManagerAgent> systemBarChangedListenerAgent_
    -sptr<WindowManagerAgent> windowUpdateListenerAgent_
    -sptr<WindowManagerAgent> windowVisibilityListenerAgent_

    -vector<sptr<IFocusChangedListener focusChangedListeners_
    -vector<sptr<ISystemBarChangedListener systemBarChangedListeners_
    -vector<sptr<IWindowUpdateListener windowUpdateListeners_
    -vector<sptr<IVisibilityChangedListener windowVisibilityListeners_    
}
class WindowAdapter #back:LightBlue {
    -sptr<IWindowManager> windowManagerServiceProxy_
    +WMError CreateWindow(window, windowProperty, surfaceNode, windowId, token)
    +WMError AddWindow(windowProperty)
    +WMError RemoveWindow(windowId)
    +WMError DestroyWindow(windowId)

    +WMError RequestFocus(windowId)
    +WMError GetAvoidAreaByType(windowId, type, avoidRect)
    +WMError SetWindowBackgroundBlur(windowId, level)
    +WMError SetAlpha(windowId, alpha)
    +WMError GetTopWindowId(mainWinId, topWinId)
    +void ProcessPointDown(windowId, isStartDrag)
    +void ProcessPointUp(windowId)
    +void MinimizeAllAppWindows(DisplayId displayId)
    +WMError ToggleShownStateForAllAppWindows()
    +WMError MaxmizeWindow(windowId)
    +WMError SetWindowLayoutMode(mode)
    +WMError UpdateProperty(sptr<windowProperty, action)
    +WMError GetSystemConfig(systemConfig)
    +WMError GetModeChangeHotZones(displayId, hotZones)

    +void RegisterWindowManagerAgent(type, windowManagerAgent)
    +void UnregisterWindowManagerAgent(type, windowManagerAgent)

    +WMError SetWindowAnimationController(controller)
    +WMError NotifyWindowTransition(from, to)
    +void ClearWindowAdapter()

    +WMError GetAccessibilityWindowInfo(windowInfo)
    +void MinimizeWindowsByLauncher(windowIds, isAnimated, finishCallback)
}
interface IWindowManager #back:Yellow

'Window -> IWindowChangeListener
'Window -> IAvoidAreaChangedListener
'Window -> IWindowDragListener
'Window -> IDisplayMoveListener
'Window -> IInputEventListener
'Window -> NotifyNativeWinDestroyFunc
'Window -> IOccupiedAreaChangeListener
'Window -> ITouchOutsideListener
'interface IWindowChangeListener {
'    void OnSizeChange(Rect rect, WindowSizeChangeReason reason)
'    void OnModeChange(WindowMode mode)
'}
'interface IAvoidAreaChangedListener {
'    void OnAvoidAreaChanged(vector<Rect> avoidAreas)
'}
'interface IWindowDragListener {
'    void OnDrag(int32_t x, int32_t y, DragEvent event)
'}
'interface IDisplayMoveListener {
'    void OnDisplayMove(DisplayId from, DisplayId to)
'}
'interface IInputEventListener {
'    void OnKeyEvent(shared_ptr<MMI::KeyEvent>& keyEvent)
'    void OnPointerInputEvent(shared_ptr<MMI::PointerEvent>& pointerEvent)
'}
'interface NotifyNativeWinDestroyFunc
'interface IOccupiedAreaChangeListener {
'    void OnSizeChange(const sptr<OccupiedAreaChangeInfo>& info)
'}
'interface ITouchOutsideListener {
'    void OnTouchOutside()
'}

@enduml