[toc]

# Window

Android中去掉ActionBar的几种方法
https://blog.csdn.net/xujingqing/article/details/51205996

Window窗口布局 --- DecorView浅析
https://www.cnblogs.com/yogin/p/4061050.html

# RenderProperties整理

## ProjectBackwards
RippleDrawable 水波纹/涟漪效果
https://www.jianshu.com/p/64a825915da9

Android应用程序UI硬件加速渲染的Display List构建过程分析
https://blog.csdn.net/luoshengyang/article/details/45943255

https://developer.android.com/reference/android/graphics/RenderNode#setProjectBackwards(boolean)


## forcedark
夜间模式
http://blog.hanschen.site/2019/10/22/android_q_dark_mode/

## OverlappingRendering
Layer? TODO

https://zhuanlan.zhihu.com/p/331927465

https://juejin.cn/post/6864005808879992845#heading-1

https://www.youtube.com/watch?v=wIy8g8yNhNk&list=PLWz5rJ2EKKc9CBxr3BVjPTPoDPLdPIFCE&t=247s

## mClipMayBeComplex
反向依赖：
RecordingCanvas::onClip
动效 layer动效 layer
TODO

## mClipBounds
https://cs.android.com/android/platform/superproject/+/master:frameworks/base/graphics/java/android/graphics/RenderNode.java;l=1728;drc=ba17c7243d0e297efbc6fb5385d6d5aa81db9152;bpv=1;bpt=1?q=nSetClipBounds&ss=android%2Fplatform%2Fsuperproject

## mRevealClip
揭露动画（RevealAnimator）的基本使用
https://blog.csdn.net/fzhhsa/article/details/102639218

https://cs.android.com/android/platform/superproject/+/master:frameworks/base/core/java/android/view/ViewAnimationUtils.java;l=71;drc=ba17c7243d0e297efbc6fb5385d6d5aa81db9152

涉及RT线程Animator机制，见类图-Animator.puml/流程图-Animator.puml，参考资料：

Android应用程序UI硬件加速渲染技术简要介绍和学习计划
https://blog.csdn.net/Luoshengyang/article/details/45601143

Android应用程序UI硬件加速渲染的动画执行过程分析
https://blog.csdn.net/Luoshengyang/article/details/46449677


# Android动画源码解析
## Choreographer.CALLBACK_ANIMATION场景梳理
### 1.RenderNodeAnimator-DelayedAnimationHelper

`frameworks/base/graphics/java/android/graphics/animation/RenderNodeAnimator.java`

管理延时RenderNodeAnimator，并启动动画

### 2.ViewRootImpl-InvalidateOnAnimationRunnable

`frameworks/base/core/java/android/view/ViewRootImpl.java`

触发view/viewRect的invalidate()：
收集invalidate的view，如scoll场景、RecycleView/ViewPager用户onTouchEvent场景触发需要invalidate
收集invalidate的viewRect，如Editor

### 3.View-postOnAnimation
修改View属性

### 4.View-scheduleDrawable

### 5.RampAnimator-mAnimationCallback

`frameworks/base/services/core/java/com/android/server/display/RampAnimator.java`

亮灭屏场景，更新亮度，不涉及HWUI
https://blog.csdn.net/FightFightFight/article/details/81320519

### 6...

## Drawable 详解
https://www.jianshu.com/p/d6c791709949

ClipDrawable: draw Clip

LayerDrawable: simple draw

AnimationDrawable: start-setFrame-scheduleSelf->run-nextFrame-setFrame

RotateDrawable

InsetDrawable

## 属性动画

ValueAnimator/ObjectAnimator

https://www.jianshu.com/p/c3636612d0c6

修改属性值：PropertyValuesHolder-setAnimatedValue

反射的方式，直接调用view的方法

ViewPropertyAnimator

https://blog.csdn.net/weixin_34268169/article/details/88123459

修改属性值：setValue中映射，直接调用view的方法


# Layer
LayerUpdateQueue
https://cs.android.com/android/platform/superproject/+/master:frameworks/base/libs/hwui/tests/unit/LayerUpdateQueueTests.cpp;l=28;drc=b81e821b3f37e2a67dba1292e1dc2715287d32f4

setLayerType
buildLayer

写
TreeInfo.layerUpdateQueue
RenderNode::pushLayerUpdate

读
SkiaPipeline::renderLayersImpl