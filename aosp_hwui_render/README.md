[toc]

# AOSP UI渲染全景图

## 类图
### View-Hwui全景
<img src="./out/类图/1.View-Graphic-Render/View-Graphic-Render.svg">

### Hwui关键类图
<img src="./out/类图/2.Graphic/RenderThread-class.svg">

### RT动画
<img src="./out/类图/4.Animator/Animator.svg">

## 时序图
### Java侧
#### View invalidate流程
<img src="./out/时序图/1.View-Invalidate/View-Invalidate.svg">

#### Choreographer入口
<img src="./out/时序图/2.Choreographer-doFrame/Choreographer-doFrame.svg">

#### ViewRootImpl doTraversal
<img src="./out/时序图/3.ViewRootImpl-doTraversal-performDraw/ViewRootImpl-doTraversal-performDraw.svg">

### Jni层
#### RecordingCanvas
<img src="./out/时序图/4.Canvas-RenderNode-draw/Canvas-RenderNode-draw.svg">

#### RenderNode
<img src="./out/时序图/5.View-RenderNode/View-RenderNode.svg">

### Native侧
#### 入口
<img src="./out/时序图/6.RenderThread-entry/Hardware-entry-syncAndDraw.svg">

#### RT同步
<img src="./out/时序图/7.RenderThread-sync/RenderThread-sync.svg">

#### RT绘制
<img src="./out/时序图/8.RenderThread-draw/RenderThread-draw.svg">
